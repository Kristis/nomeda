$(document).ready(function(){

	$('.parallax').parallax();
	
	$(".button-collapse").sideNav();

	$(document).on('mouseover', '.move',function(){
		$(this).css({
			left:(Math.random()*400)+"px",
			top:(Math.random()*400)+"px",
		});
	});

	$("#test5").on('change',function(){
		if($(this).val() > 2800){
			$("#download-button").addClass("move");
		}else{
			$("#download-button").css({
				left:"initial",
				top:"initial",
			});
			$("#download-button").removeClass("move");
		}
	});

	$("#test5").on('change mousemove',function(){
		if($(this).val() < 400){
			$("#bendr").fadeOut(1000);
			$("#kletis").fadeIn(1000);
			$("#download-button").click(function() { window.location='maziausia.html';});
		}else if($(this).val() > 400 && $(this).val() < 800){
			$("#siauliai").fadeOut(1000);
			$("#bendr").fadeIn(1000);
			$("#download-button").click(function() { window.location='mazai.html';});
		}else if($(this).val() > 800 && $(this).val() < 1000){
			$("#kaunas").fadeOut(1000);
			$("#siauliai").fadeIn(1000);
			$("#download-button").click(function() { window.location='vid.html';});
		}else if($(this).val() > 1000 && $(this).val() < 1500){
			$("#kletis").fadeOut(1000);
			$("#bendr").fadeOut(1000);
			$("#siauliai").fadeOut(1000);
			$("#kaunas").fadeIn(1000);
			$("#download-button").click(function() { window.location='daug.html';});
		}else if($(this).val() > 1500 && $(this).val() < 2800){
			$("#kaunas").fadeOut(1000);
			$("#rumai").fadeOut(1000);
			$("#loftas").fadeIn(1000);
			$("#download-button").click(function() { window.location='daugiausia.html';});
		}else if($(this).val() > 2800 ){
			$("#rumai").fadeIn(1000);
			$("#download-button").click(function() { window.location='!#';});
			}
	});	
});

$(document).ready(function(){
	$(".owl-carousel").owlCarousel({

	  	items:3,
	    loop:true,
	    margin:10,
	    nav:true,
	    dots:false,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:2
	        },
	        1000:{
	            items:3
	        }
	    },
	    navText:['ankst','kita']
	});
});