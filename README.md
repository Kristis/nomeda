### Current Version : v0.1

## Supported Browsers:
Chrome 35+, Firefox 31+, Safari 7+, IE 10+

## Quickstart:
Atsidarai naršyklėje index.html ir žiūri

## Changelog
nėra dar nieko

## Testing
ieškok klaidų ir pranešk Nomedos komandai

## Contributing
patys sau kontributinam

## Translation
Tekstas neverčiamas, nebent Deivis norės išverst ;)